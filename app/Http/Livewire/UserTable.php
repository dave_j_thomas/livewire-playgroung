<?php

namespace App\Http\Livewire;

use App\User;
use Livewire\Component;

class UserTable extends Component
{
    public $limit = 10;
    // public $users;
    public $filterName;
    public $filterEmail;
    
    
    public function limitBy($newLimit)
    {
        $this->limit = $newLimit;
    }
    
    public function render()
    {
        
        $filterName = '%'.$this->filterName.'%';
        $filterEmail = '%'.$this->filterEmail.'%';
        
        return view('livewire.user-table', [
            'users' => User::where('name', 'Like', $filterName)
                ->where('email', 'Like', $filterEmail)
                ->limit($this->limit)
                ->get(['name', 'email'])
            
            
            // ->paginate($this->limit)
        ]);
    }
}
