@extends('layouts.app')

@section('content')
    <div class="mx-auto h-1/2 flex justify-center items-center bg-gray-400">
        @livewire('search')
    </div>
@endsection
