<div>
    <div>
        <select wire:change="limitBy($event.target.value)">
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
            <option value="50">50</option>
        </select>
    </div>

    <table class="table-auto">
        <thead>
        <tr>
            <th class="px-4 py-2">Name</th>
            <th class="px-4 py-2">Email</th>
        </tr>
        <tr>
            <th class="px-4 py-2">
                <input type="text" name="name" wire:model="filterName"/>
            </th>
            <th class="px-4 py-2">
                <input type="text" name="email" wire:model="filterEmail"/>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td class="border px-4 py-2">{{$user->name}}</td>
                <td class="border px-4 py-2">{{$user->email}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{--    <div class="flex flex-1">--}}
    {{--        {{ $users->links() }}--}}
    {{--    </div>--}}

</div>
