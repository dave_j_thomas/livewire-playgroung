<div class="w-34 h-34 bg-blue-300 rounded-lg shadow-xl p-6">
    <input type="text" wire:model="searchTerm"/>

    <ul>
        @foreach($users as $user)
            <li>
                <p>{{ $user->name }}</p>
            </li>
        @endforeach
    </ul>
</div>
