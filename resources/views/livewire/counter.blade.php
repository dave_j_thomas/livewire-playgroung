<div>
    <div class="w-34 h-34 bg-blue-300 rounded-lg shadow-xl p-6">
        <button wire:click="increment">+</button>
        <h1>{{ $count }}</h1>
        <button wire:click="decrement">-</button>
    </div>
</div>
