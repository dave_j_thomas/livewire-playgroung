@extends('layouts.app')

@section('content')
    <div class="mx-auto h-full flex justify-center items-center bg-gray-400">
        @livewire('counter')
    </div>

@endsection